import pandas as pd
from Glass import *
from OneClassSVM import *

# load glass data
fullGlassData = pd.read_csv('glassdata.csv')

# make classes of different glass types
Glass01 = Glass(fullGlassData.loc[fullGlassData.type == 1], 'Glass01')
Glass02 = Glass(fullGlassData.loc[fullGlassData.type == 2], 'Glass02')
Glass03 = Glass(fullGlassData.loc[fullGlassData.type == 3], 'Glass03')
Glass05 = Glass(fullGlassData.loc[fullGlassData.type == 5], 'Glass05')
Glass06 = Glass(fullGlassData.loc[fullGlassData.type == 6], 'Glass06')
Glass07 = Glass(fullGlassData.loc[fullGlassData.type == 7], 'Glass07')

# Go through each class and get results for One Class SVM
getResultsOneClassSVM(Glass01, Glass02, Glass03, Glass05, Glass06, Glass07)
getResultsOneClassSVM(Glass02, Glass01, Glass03, Glass05, Glass06, Glass07)
getResultsOneClassSVM(Glass03, Glass01, Glass02, Glass05, Glass06, Glass07)
getResultsOneClassSVM(Glass05, Glass01, Glass02, Glass03, Glass06, Glass07)
getResultsOneClassSVM(Glass06, Glass01, Glass02, Glass03, Glass05, Glass07)
getResultsOneClassSVM(Glass07, Glass01, Glass02, Glass03, Glass05, Glass06)
