class Glass:
    def __init__(self, data, name):
        self.fullData = data
        self.numberOfLines = len(self.fullData)
        self.numberOfTrainingLines = int(round(self.numberOfLines * .8))
        self.numberOfTestingLines = self.numberOfLines - self.numberOfTrainingLines
        self.trainingData = self.fullData.iloc[0:self.numberOfTrainingLines, 1:10]
        self.testingData = self.fullData.iloc[self.numberOfTrainingLines:self.numberOfLines, 1:10]
        self.name = name
